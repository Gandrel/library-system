#ifndef UPDATEITEM_H
#define UPDATEITEM_H

#include <QDialog>

namespace Ui {
class UpdateItem;
}

class UpdateItem : public QDialog
{
    Q_OBJECT

public:
    explicit UpdateItem(QWidget *parent = nullptr);
    ~UpdateItem();

signals:
    void updateItem(QString, QString, QString, QString, QString, QString, QString, QString);

private slots:
    void on_buttonBox_accepted();

    void loadItems(QString, QString, QString, QString, QString, QString, QString, QString);

private:
    Ui::UpdateItem *ui;
};

#endif // UPDATEITEM_H
