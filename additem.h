#ifndef ADDITEM_H
#define ADDITEM_H

#include <QDialog>


namespace Ui {
class AddItem;
}

class AddItem : public QDialog
{
    Q_OBJECT

public:
    explicit AddItem(QWidget *parent = nullptr);
    ~AddItem();

signals:
    void addItem(QString, QString, QString, QString, QString, QString, QString, QString);

    void clck(QMouseEvent *);


private slots:
    void on_btnOk_Cancel_accepted();

    void setFormat();


private:
    Ui::AddItem *ui;
};

#endif // ADDITEM_H
