#ifndef VIEWWINDOW_H
#define VIEWWINDOW_H

#include <QMainWindow>
#include <QtSql>
#include <QDebug>
#include <QPixmap>
#include <QMessageBox>

#include <additem.h>
#include <updateitem.h>

namespace Ui {
class ViewWindow;
}

class ViewWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ViewWindow(QWidget *parent = nullptr);
    ~ViewWindow();

signals:
    void loadItems(QString, QString, QString, QString, QString, QString, QString, QString);

private slots:

    void logIn();

    void loadTableList();

    void loadTableItems();

    void viewItemInfo();

    void toggleEditBtns();

    void viewByGenre();

    void viewByCategory();

    void updateItem(QString, QString, QString, QString, QString, QString, QString, QString);

    void addItem(QString, QString, QString, QString, QString, QString, QString, QString);

    void deleteItem();

    void on_actionExit_triggered();

    void on_pbAdd_clicked();

    void on_pbDelete_clicked();

    void on_actionLog_out_triggered();

    void on_pbEdit_clicked();

    void on_actionUpdate_Item_triggered();

    void on_pbReload_clicked();

private:
    Ui::ViewWindow *ui;
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");

    // writableLocation() won't always return a valid string
    // sometimes, it is empty, sometimes, it does not exist
    // it also never ends with a proper directory slash, so
    //  make sure to test for validity when using it (see constructor); and
    //  make sure to prepend your directory slashes (see below)
    QString appDataDir = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QString dbPath = appDataDir + "/items.db";

    AddItem *ad;
    UpdateItem *ut;

};

#endif // VIEWWINDOW_H
