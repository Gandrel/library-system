#include "additem.h"
#include "ui_additem.h"

AddItem::AddItem(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddItem)
{
    ui->setupUi(this);
    connect(this, SIGNAL(clck(QMouseEvent *)), this, SLOT(setFormat()));
}

AddItem::~AddItem()
{
    delete ui;
}

void AddItem::on_btnOk_Cancel_accepted()
{


    if (!ui->lnCategory->text().isEmpty()){
        emit addItem(ui->lnCategory->text(), ui->lnGenre->text(),ui->lnTitle->text(), ui->lnAuthor->text(), ui->lnPublisher->text(), ui->lnDatePublished->text(), ui->lnISBN->text(), ui->pteSynopsis->toPlainText());
    }

    QString input = ui->lnDatePublished->text();


}

void AddItem::setFormat(){

    ui->lnDatePublished->setInputMask("DD/DD/DD");

}


