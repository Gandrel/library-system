#include "updateitem.h"
#include "ui_updateitem.h"


UpdateItem::UpdateItem(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UpdateItem)
{
    ui->setupUi(this);
}

UpdateItem::~UpdateItem()
{
    delete ui;
}

void UpdateItem::on_buttonBox_accepted()
{
    if (!ui->leTitle->text().isEmpty()){
        emit updateItem(ui->lineCategory->text(), ui->lineGenre->text(),ui->leTitle->text(), ui->lineAuthor->text(), ui->linePublisher->text(), ui->lineDatepbl->text(), ui->lineISBN->text(), ui->pteSynopsis->toPlainText());
    }
}

void UpdateItem::loadItems(QString ttl, QString atr, QString gnr, QString ctr, QString pbl, QString dtpbl, QString ISBN, QString snp){

    ui->leTitle->setText(ttl);
    ui->lineAuthor->setText(atr);
    ui->lineGenre->setText(gnr);
    ui->lineCategory->setText(ctr);
    ui->linePublisher->setText(pbl);
    ui->lineDatepbl->setText(dtpbl);
    ui->lineISBN->setText(ISBN);
    ui->pteSynopsis->setPlainText(snp);

}
