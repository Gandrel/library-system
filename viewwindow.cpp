#include "viewwindow.h"
#include "ui_viewwindow.h"
#include "loginwindow.h"

ViewWindow::ViewWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ViewWindow)
{
    ui->setupUi(this);

    // application cannot continue if appDataDir is empty
    if (appDataDir.isEmpty()) {
        QMessageBox::critical(this, "Critical Setup Failure",
            "An app data directory could not be found automatically. "
            "Please "
            "<a href='https://gitlab.com/Gandrel/library-system/issues'>submit</a>"
            " an issue upstream detailing your environment or setup.");
        QApplication::quit();
    }

    // create app data directory if it does not exist
    if (!QDir(appDataDir).exists()) {
        QDir().mkpath(appDataDir);
    }

    if (QFile(dbPath).exists()) {
        db.setDatabaseName(dbPath);
    }

    // warn when the database is missing
    else {
        QMessageBox::StandardButton createNewDB
            = QMessageBox::question(this, "Warning!",
            "No database could be found. Would you like to create a new database?",
            QMessageBox::Yes|QMessageBox::No);
        if (createNewDB == QMessageBox::Yes) {
            db.setDatabaseName(dbPath);
        }
        else {
            QApplication::quit();
        }
    }

    loadTableList();
    loadTableItems();

    connect(ui->tblItems, SIGNAL(clicked(QModelIndex)), this, SLOT(toggleEditBtns()));
    connect(ui->tvGenre, SIGNAL(clicked(QModelIndex)), this, SLOT(viewByGenre()));
    connect(ui->tvCategory, SIGNAL(clicked(QModelIndex)), this, SLOT(viewByCategory()));
    connect(ui->tblItems, SIGNAL(clicked(QModelIndex)), this, SLOT(viewItemInfo()));
}

ViewWindow::~ViewWindow()
{
    delete ui;
}

void ViewWindow::logIn(){
   ui->gbButtons->setEnabled(true);
   ui->pbAdd->setEnabled(true);
   ui->actionAdd_item->setEnabled(true);
   ui->actionDelete_item->setEnabled(true);
   ui->actionUpdate_Item->setEnabled(true);
}

void ViewWindow::loadTableList(){
    db.open();

    QSqlQueryModel *ctgy_md = new QSqlQueryModel();
    ctgy_md->setQuery(db.exec("SELECT DISTINCT Category FROM Books;"));
    ui->tvCategory->setModel(ctgy_md);

    QSqlQueryModel *gnr_md = new QSqlQueryModel();
    gnr_md->setQuery(db.exec("SELECT DISTINCT Genre FROM Books;"));
    ui->tvGenre->setModel(gnr_md);

    db.close();

}

void ViewWindow::loadTableItems(){
    db.open();

    QSqlQueryModel *vw_md = new QSqlQueryModel();

    vw_md->setQuery(db.exec("SELECT Title,Genre FROM Books;"));
    qDebug() << "SELECT Title, Genre FROM Books;";
    ui->tblItems->setModel(vw_md);


    db.close();
}

void ViewWindow::viewItemInfo(){
    QModelIndex itm_idx = ui->tblItems->currentIndex();
    QString itm_name = itm_idx.data(Qt::DisplayRole).toString();

    db.open();

    QSqlQuery author;
    author.exec("SELECT Author FROM Books WHERE Title = '" + itm_name + "';");
    author.next();
    QString atr = author.value(0).toString();
    ui->leAuthor->setText(atr);

    QSqlQuery Publisher;
    Publisher.exec("SELECT Publisher FROM Books WHERE Title = '" + itm_name + "';");
    Publisher.next();
    QString pbl = Publisher.value(0).toString();
    ui->lePublisher->setText(pbl);

    QSqlQuery datepbl;
    datepbl.exec("SELECT DatePublished FROM Books WHERE Title = '" + itm_name + "';");
    datepbl.next();
    QString dtpb = datepbl.value(0).toString();
    ui->leDtPbl->setText(dtpb);

    QSqlQuery ISBN;
    ISBN.exec("SELECT ISBN FROM Books WHERE Title = '" + itm_name + "';");
    ISBN.next();
    QString isbn = ISBN.value(0).toString();
    ui->leISBN->setText(isbn);

    QSqlQuery genre;
    genre.exec("SELECT Genre FROM Books WHERE Title = '" + itm_name + "';");
    genre.next();
    QString gnr = genre.value(0).toString();
    ui->leGenre->setText(gnr);

    QSqlQuery category;
    category.exec("SELECT Category FROM Books WHERE Title = '" + itm_name + "';");
    category.next();
    QString ctr = category.value(0).toString();
    ui->leCategory->setText(ctr);

    QSqlQuery title;
    title.exec("SELECT Title FROM Books WHERE Title = '" + itm_name + "';");
    title.next();
    QString ttl = title.value(0).toString();
    ui->leTitle->setText(ttl);

    QSqlQuery img_qry;
    img_qry.exec("SELECT Image FROM Books WHERE Title = '" + itm_name + "';");
    img_qry.next();
    qDebug() << "SELECT Image FROM Books WHERE Title = '" + itm_name + "';";
    QString img = img_qry.value(0).toString();
    QPixmap bc(appDataDir + "/imgs/"+ img);
    ui->vw_img->setPixmap(bc.scaled(201,221,Qt::KeepAspectRatio));

    QSqlQuery snp_qry;
    snp_qry.exec("SELECT Synopsis FROM Books WHERE Title LIKE '%" + itm_name +"%';");
    snp_qry.next();
    QString snp = snp_qry.value(0).toString();
    qDebug() << "SELECT Synopsis FROM Books WHERE Title LIKE '%" + itm_name +"%';";
    ui->tbSynopsis->setPlainText(snp);

    db.close();
}

void ViewWindow::toggleEditBtns(){

    QModelIndex idx_Items = ui->tblItems->currentIndex();
    QString itm_name = idx_Items.data(Qt::DisplayRole).toString();
    qDebug() << "Selected Item "<< itm_name <<"at row" << idx_Items.row();
    ui->pbEdit->setEnabled(true);
    ui->pbDelete->setEnabled(true);

}

void ViewWindow::viewByGenre(){

    QModelIndex idx_gnr = ui->tvGenre->currentIndex();
    QString genre = idx_gnr.data(Qt::DisplayRole).toString();
    qDebug() << "Selected Item" << genre << "at row" << idx_gnr.row();

    db.open();

    QSqlQueryModel *vwbygnr_md = new QSqlQueryModel();
    vwbygnr_md->setQuery(db.exec("SELECT Title, Genre FROM Books WHERE GENRE LIKE '%" + genre + "%';"));
    qDebug() << "SELECT Title, Genre FROM Books WHERE GENRE LIKE '%" + genre + "%';";
    ui->tblItems->setModel(vwbygnr_md);

    db.close();
}

void ViewWindow::viewByCategory(){

    QModelIndex idx_ctgry = ui->tvCategory->currentIndex();
    QString category = idx_ctgry.data(Qt::DisplayRole).toString();
    qDebug() << "Selected Item" << category << "at row" << idx_ctgry.row();

    db.open();

    QSqlQueryModel *vwbyctgry_md = new QSqlQueryModel();
    vwbyctgry_md->setQuery(db.exec("SELECT Title, Genre FROM Books WHERE Category LIKE '%" + category+ "%';"));
    qDebug() << "SELECT Title, Genre FROM Books WHERE Category LIKE '%" + category + "%';";
    ui->tblItems->setModel(vwbyctgry_md);

    db.close();
}

void ViewWindow::updateItem(QString ctgry, QString gnr, QString ttl, QString atr, QString pblshr, QString dtpbls, QString ISBN, QString snp){

    QModelIndex id = ui->tblItems->currentIndex();
    QString itm = id.data(Qt::DisplayRole).toString();
    qDebug() << "Selected item " << itm << " at row " << id.row();

    snp.remove(QChar('\''));

    db.open();
    db.exec("UPDATE Books SET Title = '" + ttl + "' WHERE Title = '" + itm + "';");
    db.close();

    db.open();
    db.exec("UPDATE Books SET Author = '" + atr + "' WHERE Title = '" + itm + "';");
    db.close();

    db.open();
    db.exec("UPDATE Books SET Publisher = '" + pblshr + "' WHERE Title = '" + itm + "';");
    db.close();

    db.open();
    db.exec("UPDATE Books SET Genre = '" + gnr + "' WHERE Title = '" + itm + "';");
    db.close();

    db.open();
    db.exec("UPDATE Books SET Category = '" + ctgry + "' WHERE Title = '" + itm + "';");
    db.close();

    db.open();
    db.exec("UPDATE Books SET DatePublished = '" + dtpbls + "' WHERE Title = '" + itm + "';");
    db.close();

    db.open();
    db.exec("UPDATE Books SET ISBN = " + ISBN + " WHERE Title = '" + itm + "';");
    db.close();

    db.open();
    db.exec("UPDATE Books SET Synopsis = '" + snp + "' WHERE Title = '" + itm + "';");
    db.close();

    loadTableList();
    loadTableItems();
    viewItemInfo();



}

void ViewWindow::addItem(QString ctgry, QString gnr, QString ttl, QString atr, QString pblshr, QString dtpbls, QString ISBN, QString snp){

    snp.remove(QChar('\''));

    db.open();


    qDebug() <<"INSERT INTO Books VALUES(" + ISBN + ",'" + ttl + "','" + atr + "','" + pblshr + "','" + dtpbls + "','" + gnr + "','" + ctgry + "','" + snp + "','');";
    db.exec("INSERT INTO Books VALUES(" + ISBN +",'" + ttl + "','" + atr + "','" + pblshr + "','" + dtpbls + "','" + gnr + "','" + ctgry + "','" + snp + "','');");
    loadTableList();
    loadTableItems();

    db.close();
}

void ViewWindow::deleteItem(){

    QModelIndex idx_dlt = ui->tblItems->currentIndex();
    QString itm = idx_dlt.data(Qt::DisplayRole).toString();
    qDebug() << "Selected item" << itm << "at row" << idx_dlt.row();

    db.open();

    QSqlQueryModel *dlt_md = new QSqlQueryModel();
    dlt_md->setQuery(db.exec("DELETE FROM Books WHERE Title = '" + itm + "';"));
    qDebug() << "DELETE FROM Books WHERE Title = '" + itm + "';";
    loadTableItems();
    loadTableList();
    viewItemInfo();

    db.close();

}

void ViewWindow::on_actionExit_triggered()
{
    this -> close();
}

void ViewWindow::on_pbAdd_clicked()
{

    ad = new AddItem(this);
    ad->setModal(true);
    ad->show();
    connect(ad, SIGNAL(addItem(QString, QString, QString, QString, QString, QString, QString, QString)), this, SLOT(addItem(QString, QString, QString, QString, QString, QString, QString, QString)));

}

void ViewWindow::on_pbDelete_clicked()
{
    deleteItem();
}

void ViewWindow::on_actionLog_out_triggered()
{
    this->close();

}

void ViewWindow::on_pbEdit_clicked()
{
    ut = new UpdateItem(this);
    ut->setModal(true);
    ut->show();
    connect(ut, SIGNAL(updateItem(QString, QString, QString, QString, QString, QString, QString, QString)), this, SLOT(updateItem(QString, QString, QString, QString, QString, QString, QString, QString)));
    connect(this, SIGNAL(loadItems(QString, QString, QString, QString, QString, QString, QString, QString)), ut, SLOT(loadItems(QString, QString, QString, QString, QString, QString, QString, QString)));
    emit loadItems(ui->leTitle->text(), ui->leAuthor->text(), ui->leGenre->text(), ui->leCategory->text(), ui->lePublisher->text(), ui->leDtPbl->text(), ui->leISBN->text(), ui->tbSynopsis->toPlainText());

}

void ViewWindow::on_actionUpdate_Item_triggered()
{
    on_pbEdit_clicked();
}

void ViewWindow::on_pbReload_clicked()
{
    loadTableItems();
}
